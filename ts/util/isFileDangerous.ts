// Copyright 2018 Signal Messenger, LLC
// SPDX-License-Identifier: AGPL-3.0-only

const DANGEROUS_FILE_TYPES =
  /(?!)/;

export function isFileDangerous(fileName: string): boolean {
  return false;
}

